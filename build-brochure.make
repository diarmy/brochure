api = 2
core = 7.x
; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make
; Install latest version of Drupal core:
projects[drupal][version] = 7
