; Drupal.org release file.
api = 2
core = 7.x

; Basic contributed modules.
projects[ctools][version] = 1.7
projects[ctools][subdir] = "contrib"
projects[entity][version] = 1.6
projects[entity][subdir] = "contrib"
projects[entityreference][version] = 1.1
projects[entityreference][subdir] = "contrib"
projects[entitycache][version] = 1.2
projects[entitycache][subdir] = "contrib"
projects[rules][version] = 2.6
projects[rules][subdir] = "contrib"
projects[views][version] = 3.11
projects[views][subdir] = "contrib"
projects[addressfield][version] = 1.0-beta5
projects[addressfield][subdir] = "contrib"
projects[features][version] = 2.0
projects[features][subdir] = "contrib"
projects[strongarm][version] = 2.0
projects[strongarm][subdir] = "contrib"
projects[libraries][version] = 2.2
projects[libraries][subdir] = "contrib"
projects[ds][version] = 2.8
projects[ds][subdir] = "contrib"
projects[panels][version] = 3.4
projects[panels][subdir] = "contrib"
projects[webform][version] = 3.20
projects[webform][subdir] = "contrib"
projects[multiple_selects][version] = 1.2
projects[multiple_selects][subdir] = "contrib"

; Other contribs.
projects[bean][version] = 1.7
projects[bean][subdir] = "contrib"
projects[colorbox][version] = 2.5
projects[colorbox][subdir] = "contrib"
projects[crumbs][version] = 2.0-beta13
projects[crumbs][subdir] = "contrib"
projects[inline_entity_form][version] = 1.5
projects[inline_entity_form][subdir] = "contrib"
projects[field_extractor][version] = 1.3
projects[field_extractor][subdir] = "contrib"
projects[service_links][version] = 2.2
projects[service_links][subdir] = "contrib"
projects[advanced_help][version] = 1.1
projects[advanced_help][subdir] = "contrib"
projects[mailsystem][version] = 2.34
projects[mailsystem][subdir] = "contrib"
projects[mimemail][version] = 1.0-beta3
projects[mimemail][subdir] = "contrib"
projects[token][version] = 1.5
projects[token][subdir] = "contrib"
projects[eva][version] = 1.2
projects[eva][subdir] = "contrib"
projects[date][version] = 2.8
projects[date][subdir] = "contrib"
projects[menu_attributes][version] = 1.0-rc2
projects[menu_attributes][subdir] = "contrib"
projects[fences][version] = "1.0"
projects[fences][subdir] = "contrib"
projects[title][version] = "1.0-alpha7"
projects[title][subdir] = "contrib"
projects[backup_migrate][version] = "2.8"
projects[backup_migrate][subdir] = "contrib"
projects[menu_block][version] = "2.3"
projects[menu_block][subdir] = "contrib"
projects[wysiwyg][version] = "2.x-dev"
projects[wysiwyg][subdir] = "contrib"
projects[google_analytics][version] = "1.4"
projects[google_analytics][subdir] = "contrib"
projects[better_formats][version] = "1.0-beta1"
projects[better_formats][subdir] = "contrib"
projects[smart_trim][version] = "1.5"
projects[smart_trim][subdir] = "contrib"
projects[jquery_update][version] = "2.4"
projects[jquery_update][subdir] = "contrib"
projects[media][version] = "1.4"
projects[media][subdir] = "contrib"
projects[media_youtube][version] = "2.0-rc4"
projects[media_youtube][subdir] = "contrib"
projects[media_vimeo][version] = "2.0"
projects[media_vimeo][subdir] = "contrib"
projects[draggableviews][version] = "2.0"
projects[draggableviews][subdir] = "contrib"
projects[views_ui_basic][version] = "1.2"
projects[views_ui_basic][subdir] = "contrib"
projects[xmlsitemap][version] = "2.0"
projects[xmlsitemap][subdir] = "contrib"
projects[auto_nodetitle][version] = "1.0"
projects[auto_nodetitle][subdir] = "contrib"

; UI improvement modules.
projects[module_filter][version] = 2.0
projects[module_filter][subdir] = "contrib"
projects[link][version] = 1.2
projects[link][subdir] = "contrib"
projects[pathauto][version] = 1.2
projects[pathauto][subdir] = "contrib"
projects[special_menu_items][version] = 2.0
projects[special_menu_items][subdir] = "contrib"
projects[chosen][version] = 2.0-beta4
projects[chosen][subdir] = "contrib"

; Base theme.
projects[mothership][version] = 2.10


; Libraries.
libraries[ckeditor][download][type]= "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.7/ckeditor_4.4.7_full.zip"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][directory_name] = "ckeditor"

libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox.git"
libraries[colorbox][destination] = "libraries"
libraries[colorbox][directory_name] = "colorbox"

libraries[jquery.bxslider][download][type] = "git"
libraries[jquery.bxslider][download][url] = "https://github.com/stevenwanderski/bxslider-4.git"
libraries[jquery.bxslider][destination] = "libraries"
libraries[jquery.bxslider][directory_name] = "jquery.bxslider"

libraries[jquery_ui_spinner][download][type] = "git"
libraries[jquery_ui_spinner][download][url] = "https://github.com/btburnett3/jquery.ui.spinner.git"
libraries[jquery_ui_spinner][destination] = "libraries"
libraries[jquery_ui_spinner][directory_name] = "jquery_ui_spinner"

libraries[jquery_expander][download][type] = "git"
libraries[jquery_expander][download][url] = "https://github.com/kswedberg/jquery-expander.git"
libraries[jquery_expander][destination] = "libraries"
libraries[jquery_expander][directory_name] = "jquery_expander"

libraries[selectnav.js][download][type] = "git"
libraries[selectnav.js][download][url] = "https://github.com/lukaszfiszer/selectnav.js.git"
libraries[selectnav.js][destination] = "libraries"
libraries[selectnav.js][directory_name] = "selectnav.js"

libraries[ie7-js][download][type] = "git"
libraries[ie7-js][download][url] = "https://github.com/roylory/ie7-js.git"
libraries[ie7-js][destination] = "libraries"
libraries[ie7-js][directory_name] = "ie7-js"

;libraries[chosen][download][type] = "file"
;libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/releases/download/v1.1.0/chosen_v1.1.0.zip"
;libraries[chosen][destination] = "libraries"
;libraries[chosen][directory_name] = "chosen"
;libraries[chosen][overwrite] = TRUE


;Brochure Features:
;libraries[brochure_features][download][type] = "git"
;libraries[brochure_features][download][url] = "https://diarmy@bitbucket.org/diarmy/brochure-features.git"
;libraries[brochure_features][destination] = "modules"
;libraries[brochure_features][directory_name] = "features"