To install run ./remake.sh

If the database has not yet been set up run the following commands:

CREATE USER '[username]'@'localhost' IDENTIFIED BY '[password]';
GRANT ALL PRIVILEGES ON [dbname].* TO '[sitename]'@'localhost';

To bootstrap site do:
drush si brochure --db-url=mysql://[username]:[password]@localhost:3306/[dbname] --site-name="[site-name]" --yes


